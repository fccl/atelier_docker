# Atelier Docker

Présentation de l'environnement docker pour faire de la virtualisation légère et scriptable. 

## Installation

Le site web de [docker](https://docker.io) contient toute l'information nécessaire dans la partie [documetation](https://docs.docker.com) du site. Dans la section [Installation](https://docs.docker.com/engine/install/) on trouve la solution pour les principaux OS.

### Debian

L'installation pour Linux est donnée pour différentes moutures. Pour Debian et dérivés le script d'installation ressemble à ceci :

    sudo apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
    sudo apt update
    sudo apt -y install docker-ce docker-ce-cli containerd.io
    sudo addgroup $USER docker

### Supprimer l'installation

- Répertoires et Fichiers. Supprimer le répertoire local (comme il contient le répertoire caché .git, la liaison avec ce projet sera supprimé)

- Docker. Supprimer le container et l'image. 


### Configuration X11

Par défaut, la sécurité réseau implique que X11 est configuré pour ne pas accepter les connexions TCP. La configuration du serveur X11 est gérée par le fichier /etc/X11/xinit/xserverrc. Mais en fait c'est au niveau du gestionnaire de connexion graphique qu'il faut agir. 

#### lightdm

Sous Debian 10 (Buster) le gestionnaire par défaut est *lightdm*. C'est donc le fichier */etc/lightdm/lightdm.conf* qu'il faut configurer avec les instructions suivantes dans la section *[Seat:*]* :

    xserver-command=X -listen tcp
    xserver-allow-tcp=true

### Installation docker-compose
La page d'installation de [docker-compose](https://docs.docker.com/compose/install/) indique qu'il faut saisir la commande suivante pour installer l'utilitaire :

    sudo curl -L "https://github.com/docker/compose/releases/download/1.29.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

Ensuite, il faut donner les droits d'exécution au fichier précédemment installé avec la commande :

    sudo chmod +x /usr/local/bin/docker-compose

## Commamdes utiles

  - Pour faire le grand ménage de printemps : **docker system prune -a**
  - Lister les images sur le poste : **docker images**
  - Supprimer une image : **docker rmi <NomOuIdImage>**
  - Construire une image : **docker build -t <NomImage> [-f <dockerfile>] context** ou context prend le nom du répertoire passe a docker pour construire l'image (souvent le répertoire courant : *.*).
  - Lancer un conteneur : **docker run [options] nomImage [commandeAexecuter]**

## Projets

### X11

Le premier projet propose est de montrer le fonctionnent de docker pour faire exécuter des applications graphiques qui tournent dans un conteneur plutôt que de devoir les installer sur l'OS de la machine. Pour se faire, on propose la création d'un script sh qui permet de créer (ou mettre à jour une image) et lance le conteneur avec les configurations nécessaires à la connexion au serveur X du poste. Pour exécuter la calculette graphique :

    chmod +x mon_appX11.sh
    ./mon_appX11.sh xcalc

On peut aussi avoir les yeux (xeyes), une horloge (xclock), un éditeur graphique (xedit)...
