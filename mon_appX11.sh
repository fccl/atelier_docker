docker build -t mon_x11 -f DockerFiles/dockerfile.x11 DockerFiles

xhost +
DISPLAY=$(hostname -I | cut -d ' ' -f 1):0.0
docker run -ti --rm --name myxeyes -e DISPLAY=$DISPLAY -v $(pwd):/home/toto mon_x11 "$@"
